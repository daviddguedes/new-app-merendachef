import { InicioPageModule } from './../pages/inicio/inicio.module';
import { FinalPageModule } from './../pages/final/final.module';
import { SaborMerendaPageModule } from './../pages/sabor-merenda/sabor-merenda.module';
import { MerendaIgualCardapioPageModule } from './../pages/merenda-igual-cardapio/merenda-igual-cardapio.module';
import { FotoMerendaPageModule } from './../pages/foto-merenda/foto-merenda.module';
import { FotoCardapioPageModule } from './../pages/foto-cardapio/foto-cardapio.module';
import { PerguntaMerendaPageModule } from './../pages/pergunta-merenda/pergunta-merenda.module';
import { PopoverAjudaPageModule } from './../pages/popover-ajuda/popover-ajuda.module';
import { PerguntaCardapioPageModule } from './../pages/pergunta-cardapio/pergunta-cardapio.module';

// import { InicioPage } from './../pages/inicio/inicio';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { Utils } from './../providers/utils';
import { MyApp } from './app.component';
import { AlterarSenhaPage } from './../pages/alterar-senha/alterar-senha';
import { ModalResetPage } from './../pages/login/modal-reset/modal-reset';
import { OfflineService } from './../providers/offline-service';
import { NetworkService } from './../providers/network-service';
import { DetalharDiaPage } from './../pages/detalhar-dia/detalhar-dia';
import { DetalharFeedPage } from './../pages/detalhar-feed/detalhar-feed';
import { OptionsRegisterPage } from './../pages/options-register/options-register';
import { EscolasService } from './../providers/escolas-service';
import { RegisterPage } from './../pages/register/register';
import { AvaliacaoService } from './../providers/avaliacao-service';
import { PontosPage } from './../pages/pontos/pontos';
import { EscolaPage } from './../pages/escola/escola';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { EnviarPage } from '../pages/enviar/enviar';
import { PerfilPage } from '../pages/perfil/perfil';
import { TimelinePage } from '../pages/timeline/timeline';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { TransferObject } from '@ionic-native/transfer';
import { Transfer } from '@ionic-native/transfer';
import { Auth } from '../providers/auth';
import { TimelineService } from '../providers/timeline-service';
import { AtualizaPerfil } from '../providers/atualiza-perfil';
import { Postagem } from '../providers/postagem';
import { Network } from '@ionic-native/network';
import { ImagemServiceProvider } from '../providers/imagem-service/imagem-service';
import { PostServiceProvider } from '../providers/post-service/post-service';
import { EnviarPostCardapioMerendaProvider } from '../providers/enviar-post-cardapio-merenda/enviar-post-cardapio-merenda';
import { EnviarPostCardapioProvider } from '../providers/enviar-post-cardapio/enviar-post-cardapio';
import { EnviarPostMerendaProvider } from '../providers/enviar-post-merenda/enviar-post-merenda';
import { EnviarPostSemCardapioMerendaProvider } from '../providers/enviar-post-sem-cardapio-merenda/enviar-post-sem-cardapio-merenda';


@NgModule({
    declarations: [
        MyApp,
        EnviarPage,
        PerfilPage,
        AlterarSenhaPage,
        EscolaPage,
        PontosPage,
        TimelinePage,
        DetalharDiaPage,
        DetalharFeedPage,
        TabsPage,
        LoginPage,
        ModalResetPage,
        RegisterPage,
        OptionsRegisterPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        InicioPageModule,
        PerguntaCardapioPageModule,
        PerguntaMerendaPageModule,
        FotoCardapioPageModule,
        FotoMerendaPageModule,
        MerendaIgualCardapioPageModule,
        SaborMerendaPageModule,
        FinalPageModule,
        PopoverAjudaPageModule,
        IonicModule.forRoot(MyApp, {
            monthNames: ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro']
        }),
        IonicStorageModule.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        EnviarPage,
        PerfilPage,
        AlterarSenhaPage,
        EscolaPage,
        PontosPage,
        TimelinePage,
        DetalharDiaPage,
        DetalharFeedPage,
        TabsPage,
        LoginPage,
        ModalResetPage,
        RegisterPage,
        OptionsRegisterPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Camera,
        FilePath,
        File,
        TransferObject,
        Transfer,
        NetworkService,
        OfflineService,
        Auth,
        Utils,
        EscolasService,
        TimelineService,
        AvaliacaoService,
        Postagem,
        AtualizaPerfil,
        Network,
        { provide: ErrorHandler, useClass: IonicErrorHandler }, ImagemServiceProvider, PostServiceProvider, EnviarPostCardapioMerendaProvider, EnviarPostCardapioProvider, EnviarPostMerendaProvider, EnviarPostSemCardapioMerendaProvider
    ]
})
export class AppModule { }
