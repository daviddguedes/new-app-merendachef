import { InicioPage } from './../pages/inicio/inicio';
import { Network } from '@ionic-native/network';
import { Auth } from './../providers/auth';
import { Storage } from '@ionic/storage';
import { TabsPage } from './../pages/tabs/tabs';
import { Component } from '@angular/core';
import { Platform, App, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';

@Component({
	templateUrl: 'app.html'
})
export class MyApp {

	rootPage: any = ''
	isOnline: boolean  

	constructor(
		platform: Platform,
		statusBar: StatusBar,
		splashScreen: SplashScreen,
		public appCtrl: App,
		private storage: Storage,
		public events: Events,
		public auth: Auth,
		public network: Network
	) {

		platform.ready().then(() => {
			statusBar.styleDefault();
			splashScreen.hide();

			this.listenEvents()

			this.storage.ready().then(r => {
				this.isLogado();
			})
		});
	}

	ionViewDidLoad() { }

	isLogado() {
		this.storage.get('token').then(res => {
			if (res == null) {
				this.rootPage = LoginPage
			} else {
				// this.rootPage = TabsPage
				this.rootPage = InicioPage
			}
		}, error => {
			this.appCtrl.getRootNav().push(LoginPage)
		}
		);
	}

	listenEvents() {
		this.events.subscribe('refresh:perfil', (token) => {
			this.auth.getUserDetails(token)
		})
	}

}
