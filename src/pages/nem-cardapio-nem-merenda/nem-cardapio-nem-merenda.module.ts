import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NemCardapioNemMerendaPage } from './nem-cardapio-nem-merenda';

@NgModule({
  declarations: [
    NemCardapioNemMerendaPage,
  ],
  imports: [
    IonicPageModule.forChild(NemCardapioNemMerendaPage),
  ],
  exports: [
    NemCardapioNemMerendaPage
  ]
})
export class NemCardapioNemMerendaPageModule {}
