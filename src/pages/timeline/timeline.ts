import { Network } from '@ionic-native/network';
import { Utils } from './../../providers/utils';
import { Storage } from '@ionic/storage';
import { DetalharDiaPage } from './../detalhar-dia/detalhar-dia';
import { DetalharFeedPage } from './../detalhar-feed/detalhar-feed';
import { AvaliacaoService } from './../../providers/avaliacao-service';
import { Component, ViewChild } from '@angular/core';
import { App, NavController, Events, LoadingController, Loading } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { TimelineService } from '../../providers/timeline-service';
import { LoginPage } from '../login/login';
import { Headers } from '@angular/http';

@Component({
	selector: 'page-timeline',
	templateUrl: 'timeline.html'
})
export class TimelinePage {

	user: any = null
	feeds: any = []
	serverUrl: string = null
	noFeeds: boolean = false
	escolaId: number
	token: string
	@ViewChild('dateTime') sTime
	loading: Loading
	isOnline: boolean

	constructor(
		public navCtrl: NavController,
		public appCtrl: App,
		public auth: Auth,
		public timelineService: TimelineService,
		public avaliacao: AvaliacaoService,
		public storage: Storage,
		public loadingCtrl: LoadingController,
		public events: Events,
		public utils: Utils,
		public network: Network
	) {
		this.serverUrl = this.auth.serverUrl
		if (this.network.type === 'none') {
			this.isOnline = false
		}else {
			this.isOnline = true
		}
	}

	ionViewWillEnter() {

	}

	ionViewDidLoad() {

		let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
			this.isOnline = false
		})

		let connectSubscription = this.network.onConnect().subscribe(() => {
			this.isOnline = true
		})

		this.storage.ready().then(() => {
			this.storage.get('token').then(token => {
				if (token !== null) {
					this.token = token
					this.getAllFeeds()
				} else {
					this.auth.logout().then(res => {
						this.appCtrl.getRootNav().setRoot(LoginPage)
					})
				}
			})
		})

		this.events.subscribe('new:feed', () => {
			this.storage.get('user').then(user => {
				this.user = user
				console.log(this.user)
				this.refreshFeed(this.user)
			})
		})
	}

	getAllFeeds() {
		this.storage.get('user').then(user => {
			this.user = user
			console.log(this.user)
			this.getFeed(this.user)
		})
	}

	changeColor(valor): string {
		return this.utils.changeColor(valor)
	}

	changeIcon(valor): string {
		return this.utils.changeIcon(valor)
	}

	changeText(valor): string {
		return this.utils.changeText(valor)
	}

	// Retorna um token válido
	validToken(cb) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', 'Bearer ' + this.token);
		this.auth.tokenRefresh(headers).subscribe(response => {
			console.log(response)
			return cb()
		}, error => {
			console.log(error)
			return cb()
		})
	}

	getFeed(user) {
		this.escolaId = user.aluno.escola.esc_id
		this.loading = this.loadingCtrl.create({
			content: 'Carregando...',
		});
		this.loading.present();

		if (this.isOnline) {
			this.auth.validToken(_token => {
				this.timelineService.getFeeds(this.escolaId, _token)
					.subscribe(response => {
						if (Object.keys(response).length > 0) {
							this.storage.set('feedsOff', response)
							let tmpFeeds = this.countConferir(response)
							let t = this.arrLength(tmpFeeds)
							this.feeds = []
							for (let i = 0; i < t; i++) {
								this.feeds.push(tmpFeeds[i])
							}
							this.noFeeds = false
							this.loading.dismissAll()
						} else {
							this.feeds = []
							this.noFeeds = true
							this.loading.dismissAll()
						}
					}, err => {
						console.log(err)
						this.feeds = []
						this.noFeeds = true
						this.loading.dismissAll()
					}
					)
			})
		} else {
			this.getFeedsOffline()
		}

	}

	private getFeedsOffline() {
		this.storage.get('feedsOff').then(response => {
			console.log('offline: ', response)
			if (Object.keys(response).length > 0) {
				let tmpFeeds = this.countConferir(response)
				let t = this.arrLength(tmpFeeds)
				this.feeds = []
				for (let i = 0; i < t; i++) {
					this.feeds.push(tmpFeeds[i])
				}
				this.noFeeds = false
				this.loading.dismissAll()
			} else {
				this.feeds = []
				this.noFeeds = true
				this.loading.dismissAll()
			}
		}, err => {
			console.log(err)
			this.feeds = []
			this.noFeeds = true
			this.loading.dismissAll()
		})
	}

	refreshFeed(user) {
		this.escolaId = user.aluno.escola.esc_id
		this.auth.validToken(_token => {
			this.timelineService.getFeeds(this.escolaId, _token).subscribe(
				response => {
					if (Object.keys(response).length > 0) {
						let tmpFeeds = this.countConferir(response)
						let t = this.arrLength(tmpFeeds)
						this.feeds = []
						for (let i = 0; i < t; i++) {
							this.feeds.push(tmpFeeds[i])
						}
						this.storage.set('feedsOff', this.feeds)
						this.noFeeds = false
					} else {
						this.feeds = []
						this.noFeeds = true
					}
				}, err => {
					console.log(err)
					this.feeds = []
					this.noFeeds = true
				}
			)
		})
	}

	private arrLength(arr) {
		let tam = arr.length
		let feedsTam = this.feeds.length
		if (tam < 30 || feedsTam > tam) return tam
		if (tam >= (feedsTam + 30)) {
			return feedsTam + 30
		} else if (tam < (feedsTam + 30)) {
			return tam - feedsTam
		}
	}

	private countConferir(response) {
		let arr: any = []
		for (let i of Object.keys(response)) {
			let a = 0
			let b = 0
			let c = 0
			let d = 0
			for (let o of response[i]) {
				let res = o.pos_conferir
				switch (res) {
					case 0:
						a++
						break;
					case 1:
						b++
						break;
					case 2:
						c++
						break;
					case 3:
						d++
						break;
					default:
						break;
				}
			}
			arr.push({ key: i, data: response[i], relatorio: { igual: a, diferente: b, faltou: c, semCardapio: d } })
		}
		return arr
	}

	logout() {
		this.auth.logout().then(() => {
			this.appCtrl.getRootNav().push(LoginPage)
		});
	}

	detalharFeed(feed) {
		this.navCtrl.push(DetalharFeedPage, { feed: feed })
	}

	doRefresh($event) {
		this.getFeed(this.user)
		$event.complete()
	}

	detalharDia(feed) {
		this.navCtrl.push(DetalharDiaPage, { feed: feed })
	}

	private getMore(index, max) {
		return new Promise((resolve, reject) => {
			this.storage.get('token').then(_token => {
				this.timelineService.getFeeds(this.escolaId, _token).subscribe(response => {
					if (response.error && response.new) { // Refresh token
						this.auth.tokenRefresh(response.new)
						this.getMore(index, max)
					}

					if (Object.keys(response).length > 0) {
						let tmpFeeds = this.countConferir(response)
						let t = this.arrLength(tmpFeeds)
						this.feeds = []
						for (let i = 0; i < t; i++) {
							this.feeds.push(tmpFeeds[i])
						}
						this.noFeeds = false
					} else {
						this.noFeeds = true
					}
					resolve()
				})
			})
		})
	}

	doInfinite(infiniteScroll) {
		let indexAtual = this.feeds.length
		if (indexAtual <= 30) return infiniteScroll.complete()

		this.getMore(indexAtual, 30).then(() => {
			infiniteScroll.complete()
		})
	}

	openStart() {
		this.sTime.open();
	}

	getDateValue(date) {
		let escola_id = this.user.aluno.escola.esc_id
		this.loading = this.loadingCtrl.create({
			content: 'Carregando...',
		});
		this.loading.present();

		this.timelineService.getFeedsByDay(date, escola_id).then(response => {
			if (Object.keys(response).length > 0) {
				let tmpFeeds = this.countConferir(response)
				let t = this.arrLength(tmpFeeds)
				this.feeds = []
				for (let i = 0; i < t; i++) {
					this.feeds.push(tmpFeeds[i])
				}
				this.loading.dismissAll()
			} else {
				this.loading.dismissAll()
				this.utils.showAlert('Ops...', 'Não houve postagem na data selecionada. Tente outra.')
			}
		}, error => this.loading.dismissAll())
	}

}



