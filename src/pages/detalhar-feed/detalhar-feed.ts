import { Network } from '@ionic-native/network';
import { Utils } from './../../providers/utils';
import { Postagem } from './../../providers/postagem';
import { Storage } from '@ionic/storage';
import { AvaliacaoService } from './../../providers/avaliacao-service';
import { Auth } from './../../providers/auth';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Component({
    selector: 'page-detalhar-feed',
    templateUrl: 'detalhar-feed.html'
})
export class DetalharFeedPage {

    feed: any = {}
    serverUrl: string = ""
    user: any
    token: string = null
    isOnline: boolean

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public auth: Auth,
        public alertCtrl: AlertController,
        public avaliacao: AvaliacaoService,
        public storage: Storage,
        public postService: Postagem,
        public utils: Utils,
        public network: Network
    ) {

        this.feed = this.navParams.get('feed')
    }

    alerts() {
        console.log(this.network.type)
        if (this.network.type === 'none') {
            this.isOnline = false
            this.utils.showAlert('Offline', 'Não é possível visualizar as imagens nem denunciar uma postagem sem estar conectado a internet')
        }else {
            this.isOnline = true
        }
    }

    ionViewWillEnter() {
        this.alerts()
    }

    ionViewDidLoad() {
        this.getUser()
        this.serverUrl = this.auth.serverUrl

        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
            this.isOnline = false
        })

        let connectSubscription = this.network.onConnect().subscribe(() => {
            this.isOnline = true
        })
    }

    // deletePostFaltandoFoto() {
    //     let merenda = this.feed.pos_merenda
    //     let cardapio = this.feed.pos_cardapio

    //     if (merenda == null || merenda == "" || cardapio == null || cardapio == "") {
    //         this.auth.getValidToken(token => {
    //             this.postService.deletePost(this.feed.pos_id, token).then(
    //                 res => { this.navCtrl.pop() },
    //                 err => { this.navCtrl.pop() }
    //             )
    //         })
    //     }
    // }

    getUser() {
        this.storage.get('user').then(
            user => {
                let _user = user
                if (!_user || _user === null) return this.utils.showAlert('Erro!', 'Houve um erro inesperado.')
                this.user = _user
            }, err => {
                console.log(err)
                return this.utils.showAlert('Erro!', 'Houve um erro inesperado.')
            }
        )
    }

    showConfirm(feed) {
        if (this.isOnline) {
            let confirm = this.alertCtrl.create({
                title: 'Observacão',
                message: 'Deseja informar que a foto da merenda ou do cardápio é enganosa?',
                buttons: [
                    {
                        text: 'Cancelar',
                        handler: () => {
                            console.log('Disagree clicked');
                        }
                    },
                    {
                        text: 'Sim',
                        handler: () => {
                            this.getUser()
                            this.auth.validToken(_token => {
                                this.avaliacao.enviarAvaliacao(feed, this.user.id, _token).then(
                                    ok => { this.utils.showAlert('Obrigado!', 'Sua observacão será analisada.') },
                                    erro => { this.utils.showAlert('Erro!', erro._body) }
                                )
                            })
                        }
                    }
                ]
            });
            confirm.present();
        }
    }

    changeColor(valor): string {
        return this.utils.changeColor(valor)
    }

    changeIcon(valor): string {
        return this.utils.changeIcon(valor)
    }

    changeText(valor): string {
        return this.utils.changeText(valor)
    }

    // Retorna um token válido
    validToken(cb) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + this.token);
        this.auth.tokenRefresh(headers).subscribe(response => {
            console.log(response)
            return cb()
        }, error => {
            console.log(error)
            return cb()
        })
    }

}
