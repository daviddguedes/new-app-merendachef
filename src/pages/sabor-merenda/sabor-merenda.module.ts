import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaborMerendaPage } from './sabor-merenda';

@NgModule({
  declarations: [
    SaborMerendaPage,
  ],
  imports: [
    IonicPageModule.forChild(SaborMerendaPage),
  ],
  exports: [
    SaborMerendaPage
  ]
})
export class SaborMerendaPageModule {}
