import { PopoverAjudaPage } from './../popover-ajuda/popover-ajuda';
import { Utils } from './../../providers/utils';
import { Storage } from '@ionic/storage';
import { FinalPage } from './../final/final';
import { InicioPage } from './../inicio/inicio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-sabor-merenda',
	templateUrl: 'sabor-merenda.html',
})
export class SaborMerendaPage {

	post: any = {}
	pontos: number = null
	gostoMerenda: number = null

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private storage: Storage,
		private utils: Utils,
		private popoverCtrl: PopoverController
		) {
	}

	ionViewDidLoad() {
		this.storage.get('post').then(post => {
			this.post = post
			this.pontos = post.pontos
		})
	}

	avancar(option) {
		if (option == 0) {
			this.storage.remove('post')
			this.navCtrl.setRoot(InicioPage)
		} else if (option == 1) {
			if (this.gostoMerenda != null) {
				if (this.gostoMerenda == 0) {
					this.post.gostoMerenda = 0
				} else if (this.gostoMerenda == 1) {
					this.post.gostoMerenda = 1
				} else if (this.gostoMerenda == 2) {
					this.post.gostoMerenda = 2
				}

				this.post.pontos = this.pontos + 5

				console.log("Post em Sabor: ", this.post)

				this.storage.set('post', this.post)

				this.navCtrl.setRoot(FinalPage)
			} else {
				this.utils.presentToast("Selecione uma das opções")
			}
		}
	}

	ajuda(myEvent) {
		let popover = this.popoverCtrl.create(PopoverAjudaPage, {
			option: 6
		});
		popover.present({
			ev: myEvent
		});
	}

}
