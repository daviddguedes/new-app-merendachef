import { Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AlterarSenhaPage } from './../alterar-senha/alterar-senha';
import { PontosPage } from './../pontos/pontos';
import { EscolaPage } from './../escola/escola';
import { Component } from '@angular/core';
import { NavController, App, Events } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { AtualizaPerfil } from '../../providers/atualiza-perfil';
import { LoginPage } from '../login/login';

@Component({
	selector: 'page-perfil',
	templateUrl: 'perfil.html'
})
export class PerfilPage {

	perfil: any = {};
	token: string = null

	constructor(
		public navCtrl: NavController,
		public appCtrl: App,
		public auth: Auth,
		public atPerfil: AtualizaPerfil,
		public storage: Storage,
		public events: Events
	) {


	}

	ionViewWillEnter() {
		this.getPerfil()
	}

	ionViewDidLoad() {
		this.getPerfil()

		this.storage.get('token').then( _token => {
			this.token = _token
		})

		AtualizaPerfil.atualizouPerfil.subscribe('post:created', () => {
			this.getPerfil()
		})
	}

	getPerfil() {
		this.storage.ready().then(() => {
			this.storage.get('user').then(
				user => {
					if (user !== null) {
						this.perfil = user
					} else {
						this.logout()
					}
				},
				err => { console.log(err) }
			)
		})
	}

	logout() {
		this.auth.logout().then(() => {
			this.appCtrl.getRootNav().setRoot(LoginPage)
		});
	}

	verEscola() {
		this.navCtrl.push(EscolaPage, { escola: this.perfil.aluno.escola })
	}

	verPontos() {
		this.navCtrl.push(PontosPage, { aluno: this.perfil })
	}

	alterarSenha() {
		this.navCtrl.push(AlterarSenhaPage, { id: this.perfil.id })
	}

	// Retorna um token válido
	validToken(cb) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', 'Bearer ' + this.token);
		this.auth.tokenRefresh(headers).subscribe(response => {
			console.log(response)
			return cb()
		}, error => {
			console.log(error)
			return cb()
		})
	}


}
