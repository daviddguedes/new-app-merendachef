import { LoginPage } from './../login/login';
import { Auth } from './../../providers/auth';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, App, Events } from 'ionic-angular';
import { AtualizaPerfil } from '../../providers/atualiza-perfil';

@Component({
	selector: 'page-pontos',
	templateUrl: 'pontos.html'
})
export class PontosPage {

	perfil: any = {}
	escola: any = {}
	cidade: any = {}

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public storage: Storage,
		public auth: Auth,
		public appCtrl: App,
		public events: Events
	) {
		// this.perfil = this.navParams.get('aluno')
	}

	ionViewDidLoad() {
		this.getPerfil()
		AtualizaPerfil.atualizouPerfil.subscribe('post:created', () => {
			this.getPerfil()
		})
	}

	ionViewWillEnter() {
		this.getPerfil()
	}

	getPerfil() {
		this.storage.ready().then( () => {
			this.storage.get('user').then(
				user => {
					if (user !== null) {
						this.perfil = user
						this.escola = this.perfil.aluno.escola
						this.cidade = this.perfil.aluno.escola.endereco.cidade
					} else {
						this.logout()
					}
				},
				err => { console.log(err) }
			)
		})
	}

	logout() {
		this.auth.logout().then(() => {
			this.appCtrl.getRootNav().setRoot(LoginPage)
		});
	}

}
