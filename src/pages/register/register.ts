import { Utils } from './../../providers/utils';
import { LoginPage } from './../login/login';
import { Auth } from './../../providers/auth';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { NgForm } from '@angular/forms';

@Component({
	selector: 'page-register',
	templateUrl: 'register.html'
})
export class RegisterPage {

	errors: boolean
	errorMessage: any
	escola: number = null
	loading: Loading
	register: { nome?: string, email?: string, password?: string, repeat_password?: string } = {};
	submitted = false;
	senhasIguais = true

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public auth: Auth,
		public loadingCtrl: LoadingController,
		public utils: Utils
	) {

		this.escola = this.navParams.get('escola')

	}

	ionViewDidLoad() {
		if (this.escola === null) this.navCtrl.pop()
	}

	voltarLogin() {
		this.escola = null
		this.navCtrl.push(LoginPage)
	}

	doRegister(form: NgForm) {
		this.submitted = true;

		if (form.valid && this.escola !== null && this.register.password == this.register.repeat_password) {
			this.senhasIguais = true
			let credentials = {
				escola_id: this.escola,
				name: this.register.nome,
				email: this.register.email,
				password: this.register.password				
			}

			// let repeat_password = this.register.repeat_password

			this.loading = this.loadingCtrl.create({
				content: 'Enviando...',
			});
			this.loading.present();

			this.auth.register(credentials).then(
				result => {
					this.loading.dismissAll()
					this.utils.showAlert("Sucesso!", "Cadastrado com sucesso! \n Faca login.")
					this.navCtrl.push(LoginPage)
				},
				error => {
					let mensagem = ""
					if (error.status == 422) {
						this.loading.dismissAll()
						for (let err of Object.keys(error.json())) {
							mensagem += error.json()[err] + '\n'
						}
						this.utils.showAlert('Erro!', mensagem)
					} else {
						this.loading.dismissAll()
						this.utils.showAlert("Erro!", "Desculpe. Houve um erro desconhecido.")
					}
					this.loading.dismissAll()
				}
			);
		}else {
			this.senhasIguais = false
		}
	}

}
