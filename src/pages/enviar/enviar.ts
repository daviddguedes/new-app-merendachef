import { Utils } from './../../providers/utils';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';
import { NetworkService } from './../../providers/network-service';
import { TimelineService } from './../../providers/timeline-service';
import { Component } from '@angular/core';
import {
	NavController,
	App,
	ActionSheetController,
	Platform,
	LoadingController,
	Loading,
	Events
} from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { AtualizaPerfil } from '../../providers/atualiza-perfil';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Postagem } from '../../providers/postagem';

declare var cordova: any;

@Component({
	selector: 'page-enviar',
	templateUrl: 'enviar.html'
})
export class EnviarPage {
	token: string = null
	user: any
	hoje: any
	aluno: any
	escola: any
	imagemPrato: string = null
	imagemCardapio: string = null
	loading: Loading
	conferirItens: any = null
	erros: any = {}
	isOnline: boolean
	testePostStorage: any = null
	postouHoje: any = null

	constructor(
		public navCtrl: NavController,
		public appCtrl: App,
		public tService: TimelineService,
		public auth: Auth,
		public atPerfil: AtualizaPerfil,
		public post: Postagem,
		private camera: Camera,
		public actionSheetCtrl: ActionSheetController,
		public platform: Platform,
		public loadingCtrl: LoadingController,
		public filePath: FilePath,
		public file: File,
		public fileTransfer: TransferObject,
		public transfer: Transfer,
		private networkService: NetworkService,
		private network: Network,
		private storage: Storage,
		public postService: Postagem,
		public events: Events,
		public utils: Utils
	) {

	}

	ionViewDidLoad() {
		this.storage.get('token').then(_token => {
			this.token = _token
		})

		this.storage.get('user').then(user => {
			this.user = user
		})
	}

	public takePicture(sourceType, tipo) {
		const options: CameraOptions = {
			quality: 100,
			sourceType: sourceType,
			saveToPhotoAlbum: false,
			correctOrientation: true,
			targetWidth: 620,
			targetHeight: 620,
			allowEdit: true
		};

		this.camera.getPicture(options).then((imagePath) => {
			if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
				this.filePath.resolveNativePath(imagePath)
					.then(filePath => {
						let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
						let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
						this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), tipo);
					});
			} else {
				var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
				var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
				this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), tipo);
			}
		}, (err) => {
			this.utils.presentToast('Erro ao selecionar imagem.');
		});
	}

	public takePictureGalery(sourceType, tipo) {
		const options: CameraOptions = {
			quality: 100,
			sourceType: sourceType,
			saveToPhotoAlbum: true,
			correctOrientation: true,
			targetWidth: 620,
			targetHeight: 620,
			allowEdit: true
		};

		this.camera.getPicture(options).then((imagePath) => {
			this.utils.showAlert('Sem conexão', 'Sempre que estiver sem conexão você pode salvar as fotos na galeria e enviar quando cosneguir se conectar a internet.')
		}, (err) => {
			this.utils.presentToast('Erro ao selecionar imagem.');
		});
	}

	public presentActionSheet(tipo) {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Selecione a imagem',
			buttons: [
				{
					text: 'Buscar na Galeria',
					handler: () => {
						this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, tipo);
					}
				},
				{
					text: 'Tirar foto',
					handler: () => {
						this.takePicture(this.camera.PictureSourceType.CAMERA, tipo);
					}
				},
				{
					text: 'Cancelar',
					role: 'cancel'
				}
			]
		});

		let actionSheetOffline = this.actionSheetCtrl.create({
			title: 'Sem conexão...',
			buttons: [
				{
					text: 'Salvar na galeria',
					handler: () => {
						this.takePictureGalery(this.camera.PictureSourceType.CAMERA, tipo);
					}
				},
				{
					text: 'Cancelar',
					role: 'cancel'
				}
			]
		});

		this.haveConnection((con) => {
			if (con) {
				actionSheet.present()
			} else {
				actionSheetOffline.present()
			}
		})
	}

	public haveConnection(cb) {
		if (this.network.type !== 'none') {
			this.isOnline = true
			return cb(true)
		} else {
			this.isOnline = false
			return cb(false)
		}
	}

	private createFileName() {
		var d = new Date(),
			n = d.getTime(),
			newFileName = n + ".jpg";
		return newFileName;
	}

	private copyFileToLocalDir(namePath, currentName, newFileName, tipo) {
		this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
			if (tipo == 0) {
				this.imagemPrato = newFileName;
			} else if (tipo == 1) {
				this.imagemCardapio = newFileName;
			}
		}, error => {
			this.utils.presentToast('Erro salvando a foto.');
		});
	}

	public pathForImage(img) {
		if (img === null) {
			return '';
		} else {
			return cordova.file.dataDirectory + img;
		}
	}

	public enviarPost() {
		this.postService.jaPostouHoje().then(res => {
			this.hoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D HH:mm:ss');
			this.postouHoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D');
			this.escola = this.user.aluno.escola.esc_id;

			let url = this.auth.apiUrl + '/store-post';

			let targetPath = this.pathForImage(this.imagemPrato);
			let filename = this.imagemPrato;

			this.auth.validToken(tokenValid => {
				let options: FileUploadOptions = {
					fileKey: "file",
					fileName: filename,
					chunkedMode: false,
					mimeType: "multipart/form-data",
					headers: {
						'Authorization': 'Bearer ' + tokenValid
					},
					params: {
						'fileName': filename,
						'pos_date': this.hoje,
						'aluno_id': this.user.id,
						'escola_id': this.escola,
						'conferir': this.conferirItens
					}
				};

				const fileTransfer: TransferObject = this.transfer.create();

				this.loading = this.loadingCtrl.create({
					content: 'Enviando imagem do prato...',
				});
				this.loading.present();

				// Enviando a foto do prato
				fileTransfer.upload(targetPath, url, options).then(res => {
					this.loading.dismissAll()
					this.utils.presentToast('Imagem do prato enviada com sucesso.');

					this.loading = this.loadingCtrl.create({
						content: 'Enviando imagem do cardápio...',
					});
					this.loading.present();

					const post = res.response

					let urlUploadPost = this.auth.apiUrl + '/update-post/' + post;

					let targetPathCardapio = this.pathForImage(this.imagemCardapio);
					let filenameCardapio = this.imagemCardapio;

					// this.auth.validToken(tokenValidTwo => {
					const optionsCardapio: FileUploadOptions = {
						fileKey: "file",
						fileName: filenameCardapio,
						chunkedMode: false,
						mimeType: "multipart/form-data",
						headers: {
							'Authorization': 'Bearer ' + tokenValid
						},
						params: {
							'fileName': filenameCardapio,
							'aluno_id': this.user.id
						}
					};

					const fileTransferCardapio: TransferObject = this.transfer.create();

					// Enviando a foto do cardápio
					fileTransferCardapio.upload(targetPathCardapio, urlUploadPost, optionsCardapio).then(data1 => {
						this.loading.dismissAll()
						this.utils.presentToast('Imagens enviadas com sucesso.');

						this.imagemPrato = null
						this.imagemCardapio = null
						this.conferirItens = null
						this.testePostStorage = null

						this.storage.set('postouHoje', this.postouHoje)

						this.events.publish('new:feed')
						this.events.publish('refresh:perfil', tokenValid)
					}, err => {
						this.events.publish('fail:cardapio')
						this.loading.dismissAll()
						this.utils.presentToast('Erro ao enviar a imagem do cardápio. A postagem será deletada.');
					});

					//})//this.auth.validToken() - 2ª foto


				}, err => {
					this.loading.dismissAll()
					this.utils.presentToast('Erro ao enviar a imagem do prato.');
				});


			}) //this.auth.validToken() - 1ª foto
		}, err => {
			this.utils.showAlert('Atenção!', 'Você já postou hoje.')
		})

	}

	public informarFaltaDeCardapio() {
		this.cancelar()
		this.postService.jaPostouHoje().then(res => {
			this.loading = this.loadingCtrl.create({
				content: 'Enviando a informação...',
			});
			this.loading.present();

			this.hoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D HH:mm:ss');
			this.postouHoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D');
			this.escola = this.user.aluno.escola.esc_id;

			let post = {
				'pos_date': this.hoje,
				'aluno_id': this.user.id,
				'escola_id': this.escola,
				'conferir': 3
			}

			this.auth.promiseTokenRefresh().then(_token => {
				this.postService.faltaCardapio(post, _token).subscribe(res => {
					this.loading.dismissAll()
					this.utils.presentToast(res.data);
					this.storage.set('postouHoje', this.postouHoje)
					this.events.publish('new:feed')
					this.events.publish('refresh:perfil', _token)
				}, err => {
					this.loading.dismissAll()
					this.utils.presentToast('Verifique sua conexão');
				})
			}, err => {
				this.loading.dismissAll()
				this.utils.presentToast('Verifique sua conexão');
			})
		}, err => {
			this.utils.showAlert('Atenção!', 'Você já postou hoje.')
		})

	}

	public cancelar() {
		this.imagemPrato = null
		this.imagemCardapio = null
		this.conferirItens = null
	}

}
