import { PopoverAjudaPage } from './../popover-ajuda/popover-ajuda';
import { PostServiceProvider } from './../../providers/post-service/post-service';
import { Storage } from '@ionic/storage';
import { InicioPage } from './../inicio/inicio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, App } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-final',
	templateUrl: 'final.html',
})
export class FinalPage {

	post: any = {}

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private storage: Storage,
		private postService: PostServiceProvider,
		private popoverCtrl: PopoverController,
		private appCtrl: App) {

	}

	ionViewDidLoad() {
		this.storage.get('post').then(post => {
			this.post = post
			console.log("POST: ", this.post)
		})
	}

	postar(option) {
		if (option == 0) {
			this.storage.remove('post')
			this.navCtrl.setRoot(InicioPage)
		} else if (option == 1) {
			console.log("post em final: ", this.post)
			this.postService.enviarPostagem(this.post).then( res => {
				console.log(res)
				this.appCtrl.getRootNav().setRoot(InicioPage)
			}, err => {
				console.log(err)
			})
		}
	}

	ajuda(myEvent) {
		let popover = this.popoverCtrl.create(PopoverAjudaPage, {
			option: 4
		});
		popover.present({
			ev: myEvent
		});
	}

}
