import { PostServiceProvider } from './../../providers/post-service/post-service';
import { Utils } from './../../providers/utils';
import { Auth } from './../../providers/auth';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicPageModule, IonicErrorHandler } from 'ionic-angular';
// import { IonicModule, IonicErrorHandler } from 'ionic-angular';
import { FinalPage } from './final';

@NgModule({
  declarations: [
    FinalPage,
  ],
  imports: [
    // IonicModule.forChild(FinalPage),
    IonicPageModule.forChild(FinalPage),
  ],
  exports: [
    FinalPage
  ],
  providers: [
    Auth,
    Utils,
    { provide: ErrorHandler, useClass: IonicErrorHandler }, PostServiceProvider
  ]
})
export class FinalPageModule {}
