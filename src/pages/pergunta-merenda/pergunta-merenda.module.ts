import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerguntaMerendaPage } from './pergunta-merenda';

@NgModule({
  declarations: [
    PerguntaMerendaPage,
  ],
  imports: [
    IonicPageModule.forChild(PerguntaMerendaPage),
  ],
  exports: [
    PerguntaMerendaPage
  ]
})
export class PerguntaMerendaPageModule {}
