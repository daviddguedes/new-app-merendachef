import { PopoverAjudaPage } from './../popover-ajuda/popover-ajuda';
import { Storage } from '@ionic/storage';
import { FotoMerendaPage } from './../foto-merenda/foto-merenda';
import { FinalPage } from './../final/final';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-pergunta-merenda',
	templateUrl: 'pergunta-merenda.html',
})
export class PerguntaMerendaPage {

	post: any = {}
	pontos: number = null

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private storage: Storage,
		public popoverCtrl: PopoverController) {
	}

	ionViewDidLoad() {
		this.storage.get('post').then(post => {
			this.post = post
			this.pontos = post.pontos
		})
	}

	merenda(option) {
		if (option == 0) {
			this.post.pontos = this.pontos + 100
			this.post.temMerenda = false
			this.storage.set('post', this.post)
			console.log("POST: ", this.post)
			this.navCtrl.setRoot(FinalPage)
		} else if (option == 1) {
			this.post.temMerenda = true
			this.storage.set('post', this.post)
			this.navCtrl.setRoot(FotoMerendaPage)
		}
	}

	ajuda(myEvent) {
		let popover = this.popoverCtrl.create(PopoverAjudaPage, {
			option: 2
		});
		popover.present({
			ev: myEvent
		});
	}

}
