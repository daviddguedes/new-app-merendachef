import { Utils } from './../../providers/utils';
import { Storage } from '@ionic/storage';
import { Auth } from './../../providers/auth';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';

@Component({
    selector: 'page-alterar-senha',
    templateUrl: 'alterar-senha.html'
})
export class AlterarSenhaPage {

    id: number = null
    passwordForm: FormGroup
    errors: boolean
    loading: Loading
    token: string = null

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public http: Http,
        public alertCtrl: AlertController,
        public formBuilder: FormBuilder,
        private auth: Auth,
        private storage: Storage,
        public loadingCtrl: LoadingController,
        public utils: Utils
    ) {
        // this.id = this.navParams.get('id')

        this.passwordForm = formBuilder.group({
            old_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
            new_password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
            repeat_password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
        });
    }

    ionViewDidLoad() {
        this.storage.get('token').then( _token => {
            this.token = _token
        })

        this.storage.get('user').then(_user => {
            this.id = _user.id
        })
    }

    alterarSenha() {
        if (!this.passwordForm.valid || this.id === null) {
            this.errors = true;
        } else {
            let password = {
                id: this.id,
                old: this.passwordForm.value.old_password,
                new: this.passwordForm.value.new_password
            }

            if (password.new !== this.passwordForm.value.repeat_password) {
                this.errors = false
                this.utils.showAlert('Erro!', 'As senhas não coincidem')
                this.passwordForm.reset()
            } else if (password.old === password.new) {
                this.utils.showAlert('Erro!', 'A nova senha precisa ser diferente da atual')
                this.passwordForm.reset()
            } else {
                this.loading = this.loadingCtrl.create({
                    content: 'Um momento...',
                });
                this.loading.present();

                this.auth.validToken( _token => {
                    let headers = new Headers();
                    headers.append('Content-Type', 'application/json');
                    headers.append('Authorization', 'Bearer ' + _token);

                    this.auth.saveNewPassword(password, headers).subscribe((res) => {
                        this.loading.dismissAll()
                        this.passwordForm.reset()
                        this.utils.showAlert('Sucesso!', 'Senha alterada com sucesso!')
                        this.navCtrl.popAll()
                    }, error => {

                        this.passwordForm.reset()
                        this.loading.dismissAll()
                        this.utils.showAlert('Falhou!', error.message)
                    })
                })

            }
        }
    }

    showAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    public cancelar() {
        this.navCtrl.pop()
    }

}
