import { Utils } from './../../providers/utils';
import { DetalharFeedPage } from './../detalhar-feed/detalhar-feed';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
    selector: 'page-detalhar-dia',
    templateUrl: 'detalhar-dia.html'
})
export class DetalharDiaPage {

    feed: Object = {}

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public utils: Utils
    ) {

        this.feed = this.navParams.get('feed')

    }

    changeColor(valor): string {
        return this.utils.changeColor(valor)
    }

    changeIcon(valor): string {
        return this.utils.changeIcon(valor)
    }

    changeText(valor): string {
        return this.utils.changeText(valor)
    }

    detalharFeed(feed) {
        this.navCtrl.push(DetalharFeedPage, { feed: feed })
    }

}
