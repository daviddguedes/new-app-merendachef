import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MerendaIgualCardapioPage } from './merenda-igual-cardapio';

@NgModule({
  declarations: [
    MerendaIgualCardapioPage,
  ],
  imports: [
    IonicPageModule.forChild(MerendaIgualCardapioPage),
  ],
  exports: [
    MerendaIgualCardapioPage
  ]
})
export class MerendaIgualCardapioPageModule {}
