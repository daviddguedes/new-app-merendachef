import { PopoverAjudaPage } from './../popover-ajuda/popover-ajuda';
import { Utils } from './../../providers/utils';
import { Storage } from '@ionic/storage';
import { SaborMerendaPage } from './../sabor-merenda/sabor-merenda';
import { InicioPage } from './../inicio/inicio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-merenda-igual-cardapio',
	templateUrl: 'merenda-igual-cardapio.html',
})
export class MerendaIgualCardapioPage {

	post: any = {}
	pontos: number = null
	merendaCardapio: number = null

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private storage: Storage,
		private utils: Utils,
		private popoverCtrl: PopoverController
		) {
	}

	ionViewDidLoad() {
		this.storage.get('post').then(post => {
			this.post = post
			this.pontos = post.pontos
		})
	}

	avancar(option) {
		if (option == 0) {
			this.storage.remove('post')
			this.navCtrl.setRoot(InicioPage)
		} else if (option == 1) {
			if (this.merendaCardapio != null) {
				if (this.merendaCardapio == 0) {
					this.post.pontos = this.pontos + 20
					this.post.merendaIgual = 0
				}else if (this.merendaCardapio == 1) {
					this.post.pontos = this.pontos + 10
					this.post.merendaIgual = 1
				}

				console.log("Post em Sabor: ", this.post)

				this.storage.set('post', this.post)

				this.navCtrl.setRoot(SaborMerendaPage)
			}else {
				this.utils.presentToast("Selecione uma das opções")
			}
		}
	}

	ajuda(myEvent) {
		let popover = this.popoverCtrl.create(PopoverAjudaPage, {
			option: 5
		});
		popover.present({
			ev: myEvent
		});
	}

}
