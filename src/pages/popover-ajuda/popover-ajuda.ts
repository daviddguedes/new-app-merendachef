import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover-ajuda',
  templateUrl: 'popover-ajuda.html',
})
export class PopoverAjudaPage {

  option: number

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.option = this.navParams.get('option')
  }

  ionViewDidLoad() {
    
  }

}
