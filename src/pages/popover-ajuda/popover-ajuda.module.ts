import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverAjudaPage } from './popover-ajuda';

@NgModule({
  declarations: [
    PopoverAjudaPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverAjudaPage),
  ],
  exports: [
    PopoverAjudaPage
  ]
})
export class PopoverAjudaPageModule {}
