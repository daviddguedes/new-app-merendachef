import { PopoverAjudaPage } from './../popover-ajuda/popover-ajuda';
import { Storage } from '@ionic/storage';
import { FotoCardapioPage } from './../foto-cardapio/foto-cardapio';
import { PerguntaMerendaPage } from './../pergunta-merenda/pergunta-merenda';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pergunta-cardapio',
  templateUrl: 'pergunta-cardapio.html',
})
export class PerguntaCardapioPage {

  post: any = {}

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private popoverCtrl: PopoverController) {

  }

  ionViewDidLoad() {
    this.storage.remove('post')
  }

  cardapio(option) {
    if (option == 0) {
      this.post.temCardapio = false
      this.post.pontos = 30
      this.storage.set('post', this.post)
      console.log("POST: ", this.post)
      this.navCtrl.setRoot(PerguntaMerendaPage)
    } else if (option == 1) {
      this.post.temCardapio = true
      this.storage.set('post', this.post)
      console.log("Post em pergunta cardapio: ", this.post)
      this.navCtrl.setRoot(FotoCardapioPage)
    }
  }

  ajuda(myEvent) {
    let popover = this.popoverCtrl.create(PopoverAjudaPage, {
      option: 0
    });
    popover.present({
      ev: myEvent
    });
  }

}
