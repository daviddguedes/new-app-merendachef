import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerguntaCardapioPage } from './pergunta-cardapio';

@NgModule({
  declarations: [
    PerguntaCardapioPage
  ],
  imports: [
    IonicPageModule.forChild(PerguntaCardapioPage),
  ],
  exports: [
    PerguntaCardapioPage
  ]
})
export class PerguntaCardapioPageModule {}
