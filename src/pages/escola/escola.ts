import { LoginPage } from './../login/login';
import { Auth } from './../../providers/auth';
import { Storage } from '@ionic/storage';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

declare var google;

@Component({
	selector: 'page-escola',
	templateUrl: 'escola.html'
})
export class EscolaPage {

	escola: any = {}
	lat: any = null
	lon: any = null
	@ViewChild('map') mapElement: ElementRef;
	map: any;
	perfil: any = {}

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private storage: Storage,
		private auth: Auth,
		private appCtrl: App
	) {

	}

	ionViewDidLoad() {

	}

	ngAfterViewInit() {
		this.loadMap();
	}

	loadMap() {
		this.getPerfil( (cb) => {
			let latLng = new google.maps.LatLng(this.lat, this.lon);

			let mapOptions = {
				center: latLng,
				zoom: 18,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}

			this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

			let marker = new google.maps.Marker({
				map: this.map,
				animation: google.maps.Animation.DROP,
				position: this.map.getCenter()
			});

			let content = this.escola.esc_nome;

			let infoWindow = new google.maps.InfoWindow({
				content: content
			});

			google.maps.event.addListener(marker, 'click', () => {
				infoWindow.open(this.map, marker);
			});
		})
	}

	getPerfil(cb) {
		this.storage.ready().then(() => {
			this.storage.get('user').then(
				user => {
					if (user !== null) {
						this.perfil = user
						this.escola = this.perfil.aluno.escola
						// this.escola = this.navParams.get('escola')
						this.lat = this.escola.endereco.end_latitude
						this.lon = this.escola.endereco.end_longitude
						return cb()
					} else {
						this.logout()
					}
				},
				err => { console.log(err) }
			)
		})
	}

	logout() {
		this.auth.logout().then(() => {
			this.appCtrl.getRootNav().setRoot(LoginPage)
		});
	}

}
