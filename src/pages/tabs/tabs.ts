import { Component } from '@angular/core';
import { App } from 'ionic-angular';
import { TimelinePage } from '../timeline/timeline';
import { EnviarPage } from '../enviar/enviar';
import { PerfilPage } from '../perfil/perfil';
import { Auth } from '../../providers/auth';
import { Storage } from '@ionic/storage';

@Component({
	templateUrl: 'tabs.html'
})
export class TabsPage {
	tab1Root: any = TimelinePage;
	tab2Root: any = EnviarPage;
	tab3Root: any = PerfilPage;
	errorMessage: any;
	user: any = {};

	constructor(public auth: Auth, public appCtrl: App, private storage: Storage) {
		
	}
}
