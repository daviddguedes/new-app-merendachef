import { RegisterPage } from './../register/register';
import { EscolasService } from './../../providers/escolas-service';
import { Auth } from './../../providers/auth';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';

@Component({
    selector: 'page-options-register',
    templateUrl: 'options-register.html'
})
export class OptionsRegisterPage {

    estados: any = null
    estado: any = null
    cidades: any = null
    cidade: any = null
    escolas: any = null
    escola: any = null
    loading: Loading

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public auth: Auth,
        public eService: EscolasService,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController
    ) { }

    ionViewDidLoad() {
        this.loading = this.loadingCtrl.create({
            content: 'Carregando estados...',
        });
        this.loading.present();

        this.eService.getEstados().then(
            res => {
                this.estados = res['estados']
                this.loading.dismissAll()
            },
            err => {
                this.loading.dismissAll()
                console.log(err)
            }
        )
    }

    showAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    selectCidade(estado) {
        this.loading = this.loadingCtrl.create({
            content: 'Carregando cidades...',
        });
        this.loading.present();

        this.eService.getCidades(estado).then(
            res => {
                this.cidades = res['cidades']
                this.loading.dismissAll()
            },
            err => {
                this.cidade = null
                this.estado = null
                this.loading.dismissAll()
                this.showAlert('Erro!', err.json().error)
            }
        )
    }

    selectEscola(cidade) {
        this.loading = this.loadingCtrl.create({
            content: 'Carregando escolas...',
        });
        this.loading.present();

        this.eService.getEscolas(cidade).then(
            res => {
                this.escolas = res['escolas']
                this.loading.dismissAll()
            },
            err => {
                this.escola = null
                this.cidade = null
                this.loading.dismissAll()
                this.showAlert('Erro!', err.json().error)
            }
        )
    }

    goRegisterForm(escola) {
        if (!escola) return this.showAlert('Erro!', 'Precisa selecionar a escola.')
        this.navCtrl.push(RegisterPage, { escola: escola })
    }

}
