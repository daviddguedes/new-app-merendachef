import { ImagemServiceProvider } from './../../providers/imagem-service/imagem-service';
import { Utils } from './../../providers/utils';
import { Auth } from './../../providers/auth';
import { NgModule, ErrorHandler } from '@angular/core';
// import { IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicPageModule, IonicErrorHandler } from 'ionic-angular';
import { FotoCardapioPage } from './foto-cardapio';

@NgModule({
  declarations: [
    FotoCardapioPage,
  ],
  imports: [
    IonicPageModule.forChild(FotoCardapioPage),
  ],
  exports: [
    FotoCardapioPage
  ],
  providers: [
    Auth,
    Utils,
    { provide: ErrorHandler, useClass: IonicErrorHandler }, ImagemServiceProvider
  ]
})
export class FotoCardapioPageModule {}
