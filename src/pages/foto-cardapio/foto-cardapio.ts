import { PopoverAjudaPage } from './../popover-ajuda/popover-ajuda';
import { Utils } from './../../providers/utils';
import { ImagemServiceProvider } from './../../providers/imagem-service/imagem-service';
import { Storage } from '@ionic/storage';
import { PerguntaMerendaPage } from './../pergunta-merenda/pergunta-merenda';
import { InicioPage } from './../inicio/inicio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-foto-cardapio',
	templateUrl: 'foto-cardapio.html',
})
export class FotoCardapioPage {

	post: any = {}
	pontos: number = null
	imagemCardapio: string = null

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private storage: Storage,
		private imgService: ImagemServiceProvider,
		public utils: Utils,
		public events: Events,
		public popoverCtrl: PopoverController) {


	}

	ionViewDidLoad() {
		this.storage.ready().then(() => {
			this.storage.get('post').then(post => {
				this.post = post
				this.pontos = post.pontos
				console.log("Post: ", this.post)
			})
		})

		this.events.subscribe('fotoCardapio', () => {
			console.log("chegou a notificacao de cardapio")
			let targetPath = this.imgService.pathForImage(this.imgService.imagemCardapio);
			this.imagemCardapio = targetPath
			console.log(this.imagemCardapio)
		})
	}

	presentActionSheet() {
		this.imgService.presentActionSheet(0)
	}

	avancar(option) {
		if (option == 0) {
			this.storage.remove('post')
			this.navCtrl.setRoot(InicioPage)
		} else if (option == 1) {
			if (this.imgService.imagemCardapio != null) {
				let targetPath = this.imgService.pathForImage(this.imgService.imagemCardapio);
				let filename = this.imgService.imagemCardapio;

				this.post.nomeImagemCardapio = filename
				this.post.pathImagemCardapio = targetPath
				// this.post.nomeImagemCardapio = "nome-da-imagem.jpg"
				// this.post.pathImagemCardapio = "http://www.lorempixel.com/650/650"
				this.post.pontos = 10

				console.log("Post em Foto cardápio: ", this.post)

				this.storage.set('post', this.post)

				this.navCtrl.setRoot(PerguntaMerendaPage)
			} else {
				this.utils.presentToast("Adicione a foto do cardápio")
			}
		}
	}

	ajuda(myEvent) {
		let popover = this.popoverCtrl.create(PopoverAjudaPage, {
			option: 1
		});
		popover.present({
			ev: myEvent
		});
	}

}
