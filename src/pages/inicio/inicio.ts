import { PopoverSettingsPage } from './../popover-settings/popover-settings';
import { PerguntaCardapioPage } from './../pergunta-cardapio/pergunta-cardapio';
import { AtualizaPerfil } from './../../providers/atualiza-perfil';
import { LoginPage } from './../login/login';
import { Auth } from './../../providers/auth';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-inicio',
	templateUrl: 'inicio.html',
})
export class InicioPage {

	perfil: any = {};
	token: string = null
	pontos: string = null

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		private storage: Storage,
		public auth: Auth,
		private appCtrl: App,
		public popoverCtrl: PopoverController) {
	}

	ionViewDidLoad() {
		this.getPerfil()

		this.storage.get('token').then(_token => {
			this.token = _token
		})

		AtualizaPerfil.atualizouPerfil.subscribe('post:created', () => {
			this.getPerfil()
		})
	}

	presentPopoverSettings(myEvent) {
		let popover = this.popoverCtrl.create(PopoverSettingsPage);
		popover.present({
			ev: myEvent
		});
	}

	getPerfil() {
		this.storage.ready().then(() => {
			this.storage.get('user').then(
				user => {
					if (user !== null) {
						this.perfil = user
						this.pontos = user.total_pontos
					} else {
						this.logout()
					}
				},
				err => { console.log(err) }
			)
		})
	}

	logout() {
		this.auth.logout().then(() => {
			this.appCtrl.getRootNav().setRoot(LoginPage)
		});
	}

	comecar() {
		this.navCtrl.setRoot(PerguntaCardapioPage)
	}

}
