import { InicioPage } from './../inicio/inicio';
import { AlterarSenhaPage } from './../alterar-senha/alterar-senha';
import { EscolaPage } from './../escola/escola';
import { PontosPage } from './../pontos/pontos';
import { LoginPage } from './../login/login';
import { Auth } from './../../providers/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover-settings',
  templateUrl: 'popover-settings.html',
})
export class PopoverSettingsPage {

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private auth: Auth,
    private appCtrl: App,
    private view: ViewController) {
  }

  ionViewDidLoad() {

  }

  push(option) {
    switch (option) {
      case 0:
        this.view.dismiss()
        // this.navCtrl.setRoot(InicioPage)
        this.navCtrl.push(PontosPage)
        break;

      case 1:
        this.view.dismiss()
        // this.navCtrl.setRoot(InicioPage)
        this.navCtrl.push(EscolaPage)
        break;

      case 2:
        this.view.dismiss()
        // this.navCtrl.setRoot(InicioPage)
        this.navCtrl.push(AlterarSenhaPage)
        break;

      // case 3:
      //   this.auth.logout().then(() => {
      //     this.view.dismiss()
      //     this.appCtrl.getRootNav().setRoot(LoginPage)
      //   });
      //   break;

      default:
        break;
    }
  }

}
