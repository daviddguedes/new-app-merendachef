import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverSettingsPage } from './popover-settings';

@NgModule({
  declarations: [
    PopoverSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverSettingsPage),
  ],
  exports: [
    PopoverSettingsPage
  ]
})
export class PopoverSettingsPageModule {}
