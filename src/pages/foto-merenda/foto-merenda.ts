import { PopoverAjudaPage } from './../popover-ajuda/popover-ajuda';
import { SaborMerendaPage } from './../sabor-merenda/sabor-merenda';
import { ImagemServiceProvider } from './../../providers/imagem-service/imagem-service';
import { Storage } from '@ionic/storage';
import { Utils } from './../../providers/utils';
import { MerendaIgualCardapioPage } from './../merenda-igual-cardapio/merenda-igual-cardapio';
import { PerguntaMerendaPage } from './../pergunta-merenda/pergunta-merenda';
import { InicioPage } from './../inicio/inicio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-foto-merenda',
	templateUrl: 'foto-merenda.html',
})
export class FotoMerendaPage {

	post: any = {}
	pontos: number
	imagemMerenda: string = null

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public utils: Utils,
		private storage: Storage,
		private imgService: ImagemServiceProvider,
		public events: Events,
		public popoverCtrl: PopoverController) {
	}

	ionViewDidLoad() {
		this.storage.ready().then( () => {
			this.storage.get('post').then(post => {
				this.post = post
				this.pontos = post.pontos
			})
		})

		this.events.subscribe('fotoMerenda', () => {
			console.log("chegou a notificacao de merenda")
			let targetPath = this.imgService.pathForImage(this.imgService.imagemMerenda);
			this.imagemMerenda = targetPath
		})
	}

	presentActionSheet() {
		this.imgService.presentActionSheet(1)
	}

	avancar(option) {
		if (option == 0) {
			this.storage.remove('post')
			this.navCtrl.setRoot(InicioPage)
		} else if (option == 1) {
			if (this.imgService.imagemMerenda != null) {
				let targetPath = this.imgService.pathForImage(this.imgService.imagemMerenda);
				let filename = this.imgService.imagemMerenda;

				this.post.nomeImagemMerenda = filename
				this.post.pathImagemMerenda = targetPath
				this.post.pontos = this.pontos + 10

				console.log("Post em Foto merenda: ", this.post)

				if (this.post.temCardapio) {
					this.storage.set('post', this.post)
					this.navCtrl.setRoot(MerendaIgualCardapioPage)
				}else {
					this.post.merendaIgual = 1
					this.storage.set('post', this.post)
					this.navCtrl.setRoot(SaborMerendaPage)
				}
				
			} else {
				this.utils.presentToast("Adicione a foto da Merenda")
			}
		}
	}

	ajuda(myEvent) {
		let popover = this.popoverCtrl.create(PopoverAjudaPage, {
			option: 3
		});
		popover.present({
			ev: myEvent
		});
	}

}
