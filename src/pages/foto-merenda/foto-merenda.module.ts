import { Utils } from './../../providers/utils';
import { Auth } from './../../providers/auth';
import { ImagemServiceProvider } from './../../providers/imagem-service/imagem-service';
import { NgModule, ErrorHandler } from '@angular/core';
// import { IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicPageModule, IonicErrorHandler } from 'ionic-angular';
import { FotoMerendaPage } from './foto-merenda';

@NgModule({
  declarations: [
    FotoMerendaPage,
  ],
  imports: [
    IonicPageModule.forChild(FotoMerendaPage),
  ],
  exports: [
    FotoMerendaPage
  ],
  providers: [
    Auth,
    Utils,
    { provide: ErrorHandler, useClass: IonicErrorHandler }, ImagemServiceProvider
  ]
})
export class FotoMerendaPageModule {}
