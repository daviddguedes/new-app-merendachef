import { InicioPage } from './../inicio/inicio';
import { Utils } from './../../providers/utils';
import { ModalResetPage } from './modal-reset/modal-reset';
import { OptionsRegisterPage } from './../options-register/options-register';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController, NavParams, App, ViewController, ModalController, AlertController, LoadingController, Loading } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage {

	errors: boolean;
	token: string = null
	errorMessage: any;
	loading: Loading
	login: { email?: string, password?: string } = {};
	submitted = false;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public auth: Auth,
		private storage: Storage,
		public appCtrl: App,
		public viewCtrl: ViewController,
		public alert: AlertController,
		public modalCtrl: ModalController,
		public loadingCtrl: LoadingController,
		public utils: Utils
	) {

		this.storage.ready().then( () => {})

	}

	ionViewDidLoad() {
		this.logado()
	}

	ionViewWillEnter() {
		this.viewCtrl.showBackButton(false);
	}

	onLogin(form: NgForm) {
		this.submitted = true;

		if (form.valid) {
			let credentials = {
				email: this.login.email,
				password: this.login.password
			}

			this.loading = this.loadingCtrl.create({
				content: 'Verificando...',
			});
			this.loading.present();

			this.auth.login(credentials).subscribe(res => {
				this.loading.dismissAll()
				if (res.result == 'email ou senha errada.') {
					this.utils.showAlert('Erro!', 'Email ou senha errada!')
				} else {
					this.token = res.result;
					this.storage.set('token', this.token);
					this.storage.set('user', res.user);
					this.appCtrl.getRootNav().push(InicioPage)
				}
			}, (err) => {
				this.loading.dismissAll()
				this.utils.showAlert('Erro!', 'Erro ao fazer login')
			});
		}
	}

	onSignup() {
		this.navCtrl.push(OptionsRegisterPage);
	}

	logado() {
		this.storage.ready().then(response => {
			this.storage.get('token').then(token => {
				if (token !== null) {
					this.storage.get('user').then(user => {
						if (user !== null) return this.navCtrl.pop()
					})
				}
				return true
			}, error => {
				return this.utils.showAlert('Erro!', 'Falhou verificando se está logado')
			});
		})
	}

	resetPassword() {
		let modal = this.modalCtrl.create(ModalResetPage);
		modal.present();
	}

}
