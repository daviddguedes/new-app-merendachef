import { Auth } from './../../../providers/auth';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';

@Component({
    selector: 'page-modal-reset',
    templateUrl: 'modal-reset.html'
})
export class ModalResetPage {

    resetForm: FormGroup
    errors: boolean
    email: string = ""
    loading: Loading
    reset: { email?: string } = {};
    submitted = false;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public viewCtrl: ViewController,
        public auth: Auth,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController
    ) {
        this.resetForm = formBuilder.group({
            email: ['', Validators.compose([Validators.minLength(4), Validators.required])]
        });
    }

    ionViewDidLoad() {

    }

    doReset(form: NgForm) {
        this.submitted = true;
        if (form.valid) {
            let credentials = {
                email: this.reset.email
            }
            this.loading = this.loadingCtrl.create({
                content: 'Enviando email...',
            });
            this.loading.present();

            this.auth.emailVerify(credentials.email).then((response) => {
                this.auth.resetPassword(credentials.email).then((res) => {
                    this.loading.dismissAll()
                    credentials.email = ''
                    this.showAlert('Sucesso!', 'Enviamos um link para seu email. Verifique também na caixa de spam.')
                    this.dismiss()
                })
            }, err => {
                this.loading.dismissAll()
                if (err['data'] !== 'success') {
                    this.showAlert('Erro!', err['data'])
                }
            })
        }
    }

    // doReset() {
    //     if (!this.resetForm.valid) {
    //         this.errors = true;
    //     } else {
    //         this.loading = this.loadingCtrl.create({
    //             content: 'Enviando email...',
    //         });
    //         this.loading.present();

    //         this.errors = false;
    //         this.email = this.resetForm.value.email
    //         this.auth.emailVerify(this.email).then((response) => {
    //             this.auth.resetPassword(this.email).then((res) => {
    //                 this.loading.dismissAll()
    //                 this.resetForm.value.email = ''
    //                 this.showAlert('Sucesso!', 'Enviamos um link para seu email. Verifique também na caixa de spam.')
    //                 this.dismiss()
    //             })
    //         }, err => {
    //             this.loading.dismissAll()
    //             if (err['data'] !== 'success') {
    //                 this.showAlert('Erro!', err['data'])
    //                 this.dismiss()
    //             }
    //         })
    //     }
    // }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    showAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

}
