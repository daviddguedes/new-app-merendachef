import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';
import { Events, Platform } from 'ionic-angular';

declare var navigator: any;
declare var Connection: any;

@Injectable()
export class NetworkService {

    static desconectado = new Events();
    static conectado = new Events();

    constructor(public http: Http, private network: Network, public platform: Platform) {
        this.getNetworkInfo()
    }

    ionViewDidLoad() {

    }

    checkNetwork() {
        return new Promise((resolve, reject) => {
            if (this.network.type !== 'none') {
                resolve()
            }else {
                debugger
                reject()
            }
        })
    }

    getNetworkInfo() {
        this.network.onDisconnect().subscribe(() => {
            this.isOffline()
        });

        this.network.onConnect().subscribe(() => {
            this.isOnline()
        });
    }

    isOffline() {
        NetworkService.desconectado.publish('network:disconnected')
    }

    isOnline() {
        NetworkService.conectado.publish('network:connected')
    }


}
