import { NetworkService } from './network-service';
import { AtualizaPerfil } from './atualiza-perfil';
import { TimelineService } from './timeline-service';
import { Auth } from './auth';
import { AlertController, ToastController, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';

declare var cordova: any;

@Injectable()
export class OfflineService {

    loading: Loading
    imagemPrato: any = null
    imagemCardapio: any = null
    conferirItens: any = null
    isOnline: boolean

    constructor(
        public http: Http,
        public storage: Storage,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public auth: Auth,
        public file: File,
        public fileTransfer: TransferObject,
        public transfer: Transfer,
        public tService: TimelineService,
        public atPerfil: AtualizaPerfil,
        public networkService: NetworkService
    ) {

        this.networkService.checkNetwork().then((info) => {
            if (info == 'none') {
                this.isOnline = false
            } else {
                this.isOnline = true
            }
        })

    }

    enviarPost(posts): Promise<any> {
        let promises_array: Array<any> = [];
        for (let p of posts) {
            promises_array.push(new Promise((resolve, reject) => {
                this.enviarPostOffline(p).then(() => {
                    resolve()
                }, err => reject())
            }))
        }
        return Promise.all(promises_array)
    }

    public enviarPostOffline(post) {
        return new Promise((resolve, reject) => {
            let aluno = post.aluno
            let cardapio = post.cardapio

            if (!this.isOnline) {
                this.showAlert('Sem conexão', 'No momento você está sem conexão com a internet. Quando estiver online a postagem será enviada.')
                reject()
            }

            let url = this.auth.apiUrl + '/store-post';

            let targetPath = post.prato;
            let filename = this.createFileName();

            const options: FileUploadOptions = {
                fileKey: "file",
                fileName: filename,
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params: {
                    'fileName': filename,
                    'pos_date': post.hoje,
                    'aluno_id': post.aluno,
                    'escola_id': post.escola,
                    'conferir': post.conferir
                }
            };

            const fileTransfer: TransferObject = this.transfer.create();

            this.loading = this.loadingCtrl.create({
                content: 'Enviando imagem do prato...',
            });
            this.loading.present();

            // Enviando a foto do prato
            fileTransfer.upload(targetPath, url, options).then(res => {
                this.loading.dismissAll()
                this.presentToast('Imagem do prato enviada com sucesso.');

                this.loading = this.loadingCtrl.create({
                    content: 'Enviando imagem do cardápio...',
                });
                this.loading.present();

                const postCreated = res.response

                let urlUploadPost = this.auth.apiUrl + '/update-post/' + postCreated;

                let targetPathCardapio = cardapio;
                let filenameCardapio = this.createFileName();

                const optionsCardapio: FileUploadOptions = {
                    fileKey: "file",
                    fileName: filenameCardapio,
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params: {
                        'fileName': filenameCardapio,
                        'aluno_id': aluno
                    }
                };

                const fileTransferCardapio: TransferObject = this.transfer.create();

                // Enviando a foto do cardápio
                fileTransferCardapio.upload(targetPathCardapio, urlUploadPost, optionsCardapio).then(data1 => {
                    this.loading.dismissAll()
                    this.presentToast('Imagens enviadas com sucesso.');

                    // this.tService.atualizaFeed()
                    // this.atPerfil.getPerfil()

                    resolve()
                }, err => {
                    this.loading.dismissAll()
                    this.presentToast('Erro ao enviar a imagem do cardápio.');
                    reject()
                });

            }, err => {
                this.loading.dismissAll()
                this.presentToast('Erro ao enviar a imagem do prato.');
                reject()
            });
        })
    }

    private createFileName() {
        var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";
        return newFileName;
    }

    private presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }

    showAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

}
