import { Auth } from './auth';
import { Storage } from '@ionic/storage';
import { AlertController, ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

declare var moment: any;

@Injectable()
export class Utils {

    constructor(
        public http: Http,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public storage: Storage,
        public auth: Auth
    ) {

    }

    public momentFormatData(date, format) {
        return moment(date).format(format);
    }

    public showAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    public presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    public changeColor(valor): string {
        let cor = ""
        switch (valor) {
            case 0:
                cor = "secondary"
                break
            case 1:
                cor = "primary"
                break
            case 2:
                cor = "danger"
                break
            case 3:
                cor = "dark"
                break
            default:
                cor = "primary"
                break
        }
        return cor
    }

    public changeIcon(valor): string {
        let icon = ""
        switch (valor) {
            case 0:
                icon = "happy"
                break
            case 1:
                icon = "sad"
                break
            case 2:
                icon = "alert"
                break
            case 3:
                icon = "alert"
                break
            default:
                icon = "hand"
                break
        }
        return icon
    }

    public changeText(valor): string {
        let texto = ""
        switch (valor) {
            case 0:
                texto = "Tudo certinho!"
                break
            case 1:
                texto = "Merenda diferente do cardápio."
                break
            case 2:
                texto = "Faltou merenda!"
                break
            case 3:
                texto = "Cardápio não disponível"
                break
            default:
                texto = ""
                break
        }
        return texto
    }

    // public getValidToken(cb) {
    //     this.storage.get('token').then(
    //         _token => {
    //             this.auth.validateToken(_token).then(
    //                 valid => {
    //                     return cb(_token)
    //                 }, invalid => {
    //                     let e = invalid.json()
    //                     if (e['error'] && e['new']) {
    //                         let _newToken = e['new']
    //                         this.storage.set('token', _newToken)
    //                         return cb(_newToken)
    //                     }
    //                 }
    //             )
    //         }, err => { }
    //     )
    // }

}
