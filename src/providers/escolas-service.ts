import { Auth } from './auth';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class EscolasService {

    constructor(public http: Http, public auth: Auth) {

    }

    getEstados() {
        return new Promise((resolve, reject) => {
            this.http.get(this.auth.apiUrl + '/get-estados')
                .map(res => res.json())
                .subscribe(
                    response => resolve(response),
                    error => reject(error)
                )
        })
    }

    getCidades(estado_id) {
        return new Promise((resolve, reject) => {
            this.http.get(this.auth.apiUrl + '/get-cidades/' + estado_id)
                .map(res => res.json())
                .subscribe(
                    response => resolve(response),
                    error => reject(error)
                )
        })
    }

    getEscolas(cidade_id) {
        return new Promise((resolve, reject) => {
            this.http.get(this.auth.apiUrl + '/get-escolas/' + cidade_id)
                .map(res => res.json())
                .subscribe(
                    response => resolve(response),
                    error => reject(error)
                )
        })
    }

}
