import { NemCardapioNemMerenda } from './NemCardapioNemMerenda';
import { TemCardapioTemMerenda } from './TemCardapioTemMerenda';
import { SoMerenda } from './SoMerenda';
import { SoCardapio } from './SoCardapio';
import { Storage } from '@ionic/storage';
import { LoadingController, Loading, Events } from 'ionic-angular';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { Auth } from './../auth';
import { Utils } from './../utils';
import { Postagem } from './../postagem';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class EnviarPostMerendaProvider {

  token: string = null
  user: any
  post: any = {}
  hoje: any
  aluno: any
  escola: any
  postouHoje: any = null
  loading: Loading

  constructor(
    public http: Http,
    public events: Events,
    public postService: Postagem,
    public utils: Utils,
    public auth: Auth,
    public fileTransfer: TransferObject,
    public transfer: Transfer,
    public loadingCtrl: LoadingController,
    private storage: Storage) {

    this.storage.ready().then(() => {
      this.storage.get('token').then(_token => {
        this.token = _token
      })

      this.storage.get('user').then(user => {
        this.user = user
      })
    })

  }

  public enviarPost(post) {
    // gostoMerenda = 0, 1 ou 2
    // merendaIgual = 0 ou 1
    // nomeImagemMerenda = 324520389457233.jpg
    // pathImagemMerenda = file:///data/data
    // pontos = 30 + 0 + 5 + (10 ou 20) + 5 = 60 ou 50
    // temCardapio = false
    // temMerenda = true
    this.post = post

    return new Promise((resolve, reject) => {
      this.postService.jaPostouHoje().then(res => {
        this.hoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D HH:mm:ss');
        this.postouHoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D');
        this.escola = this.user.aluno.escola.esc_id;

        let url = this.auth.apiUrl + '/store-post';

        let targetPathMerenda = this.post.pathImagemMerenda;
        let filenameMerenda = this.post.nomeImagemMerenda;

        this.auth.validToken(tokenValid => {
          let options: FileUploadOptions = {
            fileKey: "file",
            fileName: filenameMerenda,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            headers: {
              'Authorization': 'Bearer ' + tokenValid
            },
            params: {
              'fileName': filenameMerenda,
              'pos_date': this.hoje,
              'aluno_id': this.user.id,
              'escola_id': this.escola,
              'conferir': this.post.merendaIgual, //Será igual a 1 (merenda diferente ou cardápio não encontrado)
              'sabor': this.post.gostoMerenda,
              'pontos': this.post.pontos,
              'temCardapio': false,
              'temMerenda': true
            }
          };

          const fileTransfer: TransferObject = this.transfer.create();

          this.loading = this.loadingCtrl.create({
            content: 'Enviando imagem do prato...',
          });
          this.loading.present();

          // Enviando a foto do prato
          fileTransfer.upload(targetPathMerenda, url, options).then(res => {
            this.loading.dismissAll()
            this.utils.presentToast('Imagem da merenda enviada com sucesso.');
            this.storage.set('postouHoje', this.postouHoje)
            this.events.publish('new:feed')
            this.events.publish('refresh:perfil', tokenValid)
            this.storage.remove('post')
            resolve(res)
          }, err => {
            this.loading.dismissAll()
            this.utils.presentToast('Erro ao enviar a imagem da merenda.');
            reject(err)
          });


        }) //this.auth.validToken() - foto da merenda
      }, err => {
        this.utils.showAlert('Atenção!', 'Você já postou hoje.')
        reject()
      })
    })

  }

}
