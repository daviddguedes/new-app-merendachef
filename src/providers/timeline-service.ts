import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Auth } from './auth';
import { Events } from 'ionic-angular';

@Injectable()
export class TimelineService {

	url: string = ''
	static atualizaFeed = new Events();
	userToken: any = ''

	constructor(
		public http: Http,
		public auth: Auth,
		public events: Events,
		private storage: Storage
	) {
		this.url = this.auth.apiUrl + '/get-feeds'
	}

	// getFeeds(escola_id) {
	// 	return new Promise((resolve, reject) => {
	// 		this.storage.get('token').then(token => {
	// 			this.userToken = token
	// 			let headers = new Headers();
	// 			headers.append('Content-Type', 'application/json');
	// 			headers.append('Authorization', 'Bearer ' + this.userToken);

	// 			this.http.post(this.url, { escola_id: escola_id }, { headers: headers })
	// 				.map(res => res.json())
	// 				.subscribe((response) => {
	// 					resolve(response)
	// 				}, error => {
	// 					reject(error)
	// 				})
	// 		})
	// 	});
	// }

	getFeeds(escola_id, token) {
		this.userToken = token
		let headers = new Headers()
		headers.append('Content-Type', 'application/json')
		headers.append('Authorization', 'Bearer ' + this.userToken)

		return this.http.post(this.url, { escola_id: escola_id }, { headers: headers })
			.map(res => res.json())

	}

	getFeedsByDay(day, escola_id) {
		return new Promise((resolve, reject) => {
			let headers = new Headers();
			headers.append('Content-Type', 'application/json');
			headers.append('Authorization', 'Bearer ' + this.userToken);

			this.http.post(this.auth.apiUrl + '/get-feeds-day', { day: day, escola_id: escola_id }, { headers: headers })
				.map(res => res.json())
				.subscribe((response) => {
					resolve(response)
				}, error => {
					reject(error)
				})
		})
	}

	atualizaFeed() {
		TimelineService.atualizaFeed.publish('post:created')
	}


}
