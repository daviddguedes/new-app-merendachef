import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Auth {

	public apiUrl = 'http://merendachefe.mppb.mp.br/api'
	public serverUrl = 'http://merendachefe.mppb.mp.br'

	// public apiUrl = 'http://192.168.0.126:8000/api'
	// public serverUrl = 'http://192.168.0.126:8000'

	// public apiUrl = 'http://10.2.15.89:8000/api'
	// public serverUrl = 'http://10.2.15.89:8000'

	// public apiUrl = 'http://127.0.0.1:8000/api'
	// public serverUrl = 'http://127.0.0.1:8000'

	// public apiUrl = 'http://192.168.0.10:8000/api'
	// public serverUrl = 'http://192.168.0.10:8000'

	private token: string = null;

	constructor(public http: Http, private storage: Storage, public appCtrl: App) {
		this.storage.ready().then(() => {
			this.storage.get('token').then(_token => {
				this.token = _token
			})
		})
	}

	promiseTokenRefresh() {
		return new Promise( (resolve, reject) => {
			this.storage.get('token').then( _token => {
				let headers = new Headers();
				headers.append('Content-Type', 'application/json');
				headers.append('Authorization', 'Bearer ' + _token);

				this.http.post(this.apiUrl + '/refresh', {}, { headers: headers })
					.map((res) => res.headers.toJSON())
					.subscribe( response => {
						let newToken = response.Authorization[0].substring(7)
						this.storage.set('token', newToken)
						resolve(newToken)
					}, error => { reject(error) })
			})
		})
	}

	tokenRefresh(headers) {
		return this.http.post(this.apiUrl + '/refresh', {}, { headers: headers })
			.map((res) => res.headers.toJSON())
	}

	// Retorna um token válido
	validToken(cb) {
		this.storage.get('token').then(_token => {
			let headers = new Headers();
			headers.append('Content-Type', 'application/json');
			headers.append('Authorization', 'Bearer ' + _token);
			this.tokenRefresh(headers).subscribe(response => {
				let newToken = response.Authorization[0].substring(7)
				this.storage.set('token', newToken)
				return cb(newToken)
			}, error => {
				console.log(error)
				return cb()
			})
		})
	}

	register(credentials) {
		return new Promise((resolve, reject) => {
			let body = {
				escola_id: credentials.escola_id,
				name: credentials.name,
				email: credentials.email,
				password: credentials.password
			}

			let headers = new Headers();
			headers.append('Content-Type', 'application/json');
			let options = new RequestOptions({ headers: headers });

			this.http.post(this.apiUrl + '/register', JSON.stringify(body), options)
				.subscribe(res => {
					let data = res.json();
					resolve(data);
				}, (err) => {
					reject(err);
				});
		});
	}

	login(credentials): Observable<any> {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');

		return this.http.post(this.apiUrl + '/login', credentials, { headers: headers })
			.map(res => res.json())
	}

	logout() {
		return new Promise((resolve, reject) => {
			this.storage.remove('token')
			this.storage.remove('user')
			this.storage.remove('postouHoje')
			this.storage.remove('feedsOff')
			resolve()
		})
	}

	public getUser() {
		return this.storage.get('user')
	}

	public getUserDetails(_token) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', 'Bearer ' + _token);

		return this.http.post(this.apiUrl + '/get_user_details', {}, { headers: headers })
			.map(res => res.json())
			.subscribe(res => {
				this.storage.set('user', res.result)
			}, err => {
				this.logout()
			})
	}

	emailVerify(email) {
		return new Promise((resolve, reject) => {
			this.http.get(this.apiUrl + '/email-verify/' + email).subscribe((response) => {
				resolve(response.json())
			}, err => reject(err.json()))
		})
	}

	resetPassword(email) {
		return new Promise((resolve, reject) => {
			let headers = new Headers();
			headers.append('Content-Type', 'application/json');

			this.http.post(this.serverUrl + '/password/email', { email: email }, { headers: headers }).subscribe((response) => {
				resolve()
			}, err => {
				console.log('error: ', err)
				reject(err)
			})
		})
	}

	saveNewPassword(credentials, headers) {
		return this.http.post(this.apiUrl + '/saveNewPassword', credentials, { headers: headers })
			.map(res => res.json())
	}

}
