import { Utils } from './utils';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { Auth } from './auth';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Postagem {

	constructor(
		public http: Http,
		public auth: Auth,
		public storage: Storage,
		public utils: Utils
	) {

	}

	public jaPostouHoje() {
		return new Promise((resolve, reject) => {
			this.storage.get('postouHoje').then( response => {
				resolve()
				// if (response != null) {
				// 	let hoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D')
				// 	if (response != hoje) {
				// 		resolve()
				// 	}else {
				// 		reject()
				// 	}
				// }else {
				// 	resolve()
				// }
			}, error => { reject() })
		})
	}

	storePost(aluno_id, escola_id, pos_date, cb) {
		let params = {
			aluno_id: aluno_id,
			escola_id: escola_id,
			pos_date: pos_date
		}

		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		this.http.post(this.auth.apiUrl, params, { headers: headers }).map(res => res.json()).subscribe(
			resp => {
				return cb(resp)
			}
		);
	}

	deletePost(postId, _token) {
		return new Promise((resolve, reject) => {
			let headers = new Headers();
			headers.append('Content-Type', 'application/json');
			headers.append('Authorization', 'Bearer ' + _token);
			this.http.post(this.auth.apiUrl + '/delete-post', { postId: postId }, { headers: headers }).subscribe(
				response => resolve(response),
				error => reject(error)
			)
		})
	}

	faltaCardapio(post, _token): any {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', 'Bearer ' + _token)

		return this.http.post(this.auth.apiUrl + '/store-post', post, { headers: headers })
			.map(res => res.json())
		// .catch(this.handleError);
	}

	handleError(error) {
		console.log('error: ', error);
		return Observable.throw(error || 'Server error');
	}

}
