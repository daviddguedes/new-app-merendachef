import { Utils } from './../utils';
import { FilePath } from '@ionic-native/file-path';
import {
	ActionSheetController,
	Platform,
	LoadingController,
	Loading,
	Events
} from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

declare var cordova: any;

@Injectable()
export class ImagemServiceProvider {

	imagemCardapio: string = null
	imagemMerenda: string = null

	constructor(public http: Http,
		private camera: Camera,
		public actionSheetCtrl: ActionSheetController,
		public platform: Platform,
		public loadingCtrl: LoadingController,
		public filePath: FilePath,
		public file: File,
		public events: Events,
		public utils: Utils) {



	}

	public takePicture(sourceType, tipo) {
		const options: CameraOptions = {
			quality: 100,
			sourceType: sourceType,
			saveToPhotoAlbum: false,
			correctOrientation: true,
			targetWidth: 620,
			targetHeight: 620,
			allowEdit: true
		};

		this.camera.getPicture(options).then((imagePath) => {
			if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
				this.filePath.resolveNativePath(imagePath)
					.then(filePath => {
						let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
						let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
						this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), tipo);
					});
			} else {
				var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
				var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
				this.copyFileToLocalDir(correctPath, currentName, this.createFileName(), tipo);
			}
		}, (err) => {
			this.utils.presentToast('Erro ao selecionar imagem.');
		});
	}

	public takePictureGalery(sourceType) {
		const options: CameraOptions = {
			quality: 100,
			sourceType: sourceType,
			saveToPhotoAlbum: true,
			correctOrientation: true,
			targetWidth: 620,
			targetHeight: 620,
			allowEdit: true
		};

		this.camera.getPicture(options).then((imagePath) => {
			this.utils.showAlert('Sem conexão', 'Sempre que estiver sem conexão você pode salvar as fotos na galeria e enviar quando cosneguir se conectar a internet.')
		}, (err) => {
			this.utils.presentToast('Erro ao selecionar imagem.');
		});
	}

	public presentActionSheet(tipo) {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Selecione a imagem',
			buttons: [
				{
					text: 'Buscar na Galeria',
					handler: () => {
						this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, tipo);
					}
				},
				{
					text: 'Tirar foto',
					handler: () => {
						this.takePicture(this.camera.PictureSourceType.CAMERA, tipo);
					}
				},
				{
					text: 'Cancelar',
					role: 'cancel'
				}
			]
		});

		actionSheet.present()
	}

	private createFileName() {
		var d = new Date(),
			n = d.getTime(),
			newFileName = n + ".jpg";
		return newFileName;
	}

	private copyFileToLocalDir(namePath, currentName, newFileName, tipo) {
		this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
			if (tipo == 0) {
				console.log("evento cardapio")
				this.imagemCardapio = newFileName;
				this.events.publish('fotoCardapio')
			} else if (tipo == 1) {
				console.log("evento merenda")
				this.imagemMerenda = newFileName;
				this.events.publish('fotoMerenda')
			}

		}, error => {
			this.utils.presentToast('Erro salvando a foto.');
		});
	}

	public pathForImage(img) {
		if (img === null) {
			return '';
		} else {
			return cordova.file.dataDirectory + img;
		}
	}

}
