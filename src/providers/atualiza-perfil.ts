import { Storage } from '@ionic/storage';
import { Auth } from './auth';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

@Injectable()
export class AtualizaPerfil {

	static atualizouPerfil = new Events();

	constructor(
		public http: Http,
		public auth: Auth,
		public storage: Storage,
		public events: Events
	) {
		
	}

	// getPerfil() {
	// 	this.storage.ready().then( () => {
	// 		this.storage.get('token').then((_t) => {
	// 			this.validToken(_t, (_token) => {
	// 				this.auth.getUserDetails(_token)
	// 					.subscribe(res => {
	// 						let data = res.json();
	// 						this.storage.set('user', data.result);
	// 						AtualizaPerfil.atualizouPerfil.publish('post:created')
	// 					}, (err) => {
	// 						console.log('Erro ao tentar recuperar as informações do usuário logado.')
	// 					});
	// 			})
	// 		})
	// 	})
	// }

	// validToken(_token, cb) {
	// 	this.auth.validToken(_token => {
	// 		return cb(_token)
	// 	})
	// }

}
