import { NemCardapioNemMerenda } from './NemCardapioNemMerenda';
import { TemCardapioTemMerenda } from './TemCardapioTemMerenda';
import { SoMerenda } from './SoMerenda';
import { SoCardapio } from './SoCardapio';
import { Storage } from '@ionic/storage';
import { LoadingController, Loading, Events } from 'ionic-angular';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { Auth } from './../auth';
import { Utils } from './../utils';
import { Postagem } from './../postagem';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class EnviarPostCardapioProvider {

  token: string = null
  user: any
  post: any = {}
  hoje: any
  aluno: any
  escola: any
  postouHoje: any = null
  loading: Loading

  constructor(
    public http: Http,
    public events: Events,
    public postService: Postagem,
    public utils: Utils,
    public auth: Auth,
    public fileTransfer: TransferObject,
    public transfer: Transfer,
    public loadingCtrl: LoadingController,
    private storage: Storage) {

    this.storage.ready().then(() => {
      this.storage.get('token').then(_token => {
        this.token = _token
      })

      this.storage.get('user').then(user => {
        this.user = user
      })
    })

  }

  public enviarPost(post) {
    // pontos = foto cardapio(30 ou 5) + tem merenda?(100 ou 0) + foto da merenda(5) + merenda/cardapio(10 ou 20) + sabor(5)
    // nomeImagemCardapio = 2934823492348234.jpg
    // pathImagemCardapio = file:///data/data
    // pontos = 5 + 100 = 105
    // temCardapio = true
    // temMerenda = false

    this.post = post

    return new Promise((resolve, reject) => {
      this.postService.jaPostouHoje().then(res => {
        this.hoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D HH:mm:ss');
        this.postouHoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D');
        this.escola = this.user.aluno.escola.esc_id;

        let url = this.auth.apiUrl + '/store-post';

        let targetPathCardapio = this.post.pathImagemCardapio;
        let filenameCardapio = this.post.nomeImagemCardapio;

        this.auth.validToken(tokenValid => {
          let options: FileUploadOptions = {
            fileKey: "file",
            fileName: filenameCardapio,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            headers: {
              'Authorization': 'Bearer ' + tokenValid
            },
            params: {
              'fileName': filenameCardapio,
              'pos_date': this.hoje,
              'aluno_id': this.user.id,
              'escola_id': this.escola,
              // 'conferir': this.post.merendaIgual,
              // 'sabor': this.post.gostoMerenda,
              'pontos': this.post.pontos,
              'temCardapio': true,
              'temMerenda': false
            }
          };

          const fileTransfer: TransferObject = this.transfer.create();

          this.loading = this.loadingCtrl.create({
            content: 'Enviando imagem do cardápio...',
          });
          this.loading.present();

          // Enviando a foto do prato
          this.fileTransfer.upload(targetPathCardapio, url, options).then(res => {
            this.loading.dismissAll()
            this.utils.presentToast('Imagem do cardápio enviada com sucesso.');
            this.storage.set('postouHoje', this.postouHoje)
            this.events.publish('new:feed')
            this.events.publish('refresh:perfil', tokenValid)
            this.storage.remove('post')
            resolve(res)
          }, err => {
            this.loading.dismissAll()
            this.utils.presentToast('Erro ao enviar a imagem do cardápio.');
            reject(err)
          });


        }) //this.auth.validToken() - foto do cardápio
      }, err => {
        this.utils.showAlert('Atenção!', 'Você já postou hoje.')
        reject()
      })
    })

  }

}
