import { NemCardapioNemMerenda } from './NemCardapioNemMerenda';
import { TemCardapioTemMerenda } from './TemCardapioTemMerenda';
import { SoMerenda } from './SoMerenda';
import { SoCardapio } from './SoCardapio';
import { Storage } from '@ionic/storage';
import { LoadingController, Loading, Events } from 'ionic-angular';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { Auth } from './../auth';
import { Utils } from './../utils';
import { Postagem } from './../postagem';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class EnviarPostSemCardapioMerendaProvider {

  token: string = null
  user: any
  post: any = {}
  hoje: any
  aluno: any
  escola: any
  postouHoje: any = null
  loading: Loading

  constructor(
    public http: Http,
    public events: Events,
    public postService: Postagem,
    public utils: Utils,
    public auth: Auth,
    public fileTransfer: TransferObject,
    public transfer: Transfer,
    public loadingCtrl: LoadingController,
    private storage: Storage) {

    this.storage.ready().then(() => {
      this.storage.get('token').then(_token => {
        this.token = _token
      })

      this.storage.get('user').then(user => {
        this.user = user
      })
    })

  }

  public enviarPost(post) {
    // pontos = 30 + 100 = 130
    // temCardapio = false
    // temMerenda = false
    this.post = post

    return new Promise((resolve, reject) => {
      this.postService.jaPostouHoje().then(res => {
        this.loading = this.loadingCtrl.create({
          content: 'Enviando a informação...',
        });
        this.loading.present();

        this.hoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D HH:mm:ss');
        this.postouHoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D');
        this.escola = this.user.aluno.escola.esc_id;

        let post = {
          'pos_date': this.hoje,
          'aluno_id': this.user.id,
          'escola_id': this.escola,
          // 'conferir': this.post.merendaIgual,
          // 'sabor': this.post.gostoMerenda,
          'pontos': this.post.pontos,
          'temCardapio': false,
          'temMerenda': false
        }

        this.auth.promiseTokenRefresh().then(_token => {
          this.postService.faltaCardapio(post, _token).subscribe(res => {
            this.loading.dismissAll()
            this.utils.presentToast(res.data);
            this.storage.set('postouHoje', this.postouHoje)
            this.events.publish('new:feed')
            this.events.publish('refresh:perfil', _token)
            resolve(res)
          }, err => {
            this.loading.dismissAll()
            this.utils.presentToast('Verifique sua conexão');
            reject(err)
          })
        }, err => {
          this.loading.dismissAll()
          this.utils.presentToast('Verifique sua conexão');
          reject(err)
        })

      }, err => {
        this.utils.showAlert('Atenção!', 'Você já postou hoje.')
        reject(err)
      })
    })


  }

}
