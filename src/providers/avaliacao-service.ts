import { Auth } from './auth';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class AvaliacaoService {

	userToken: any = ''

	constructor(public http: Http, private auth: Auth, private storage: Storage) {
		this.storage.get('token').then(res => {
			this.userToken = res
		})
	}

	enviarAvaliacao(avaliacao, idDeQuemTaAvaliando, token) {
		return new Promise((resolve, reject) => {
			let headers = new Headers();
			headers.append('Content-Type', 'application/json');
			headers.append('Authorization', 'Bearer ' + token);

			this.http.post(this.auth.apiUrl + '/avaliar', { "avaliacao": avaliacao }, { headers: headers })
				.subscribe(
				response => {
					resolve(response);
				}, (err) => {
					reject(err);
				});
		})
	}



}
