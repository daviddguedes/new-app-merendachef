import { EnviarPostMerendaProvider } from './../enviar-post-merenda/enviar-post-merenda';
import { EnviarPostCardapioProvider } from './../enviar-post-cardapio/enviar-post-cardapio';
import { EnviarPostSemCardapioMerendaProvider } from './../enviar-post-sem-cardapio-merenda/enviar-post-sem-cardapio-merenda';
import { EnviarPostCardapioMerendaProvider } from './../enviar-post-cardapio-merenda/enviar-post-cardapio-merenda';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

// ESTÁ PERMITINDO MAIS DE UMA POSTAGEM POR DIA PARA TESTES.

@Injectable()
export class PostServiceProvider {
	post: any = {}

	constructor(
		private postSemCardapioSemMerenda: EnviarPostSemCardapioMerendaProvider,
		private postComCardapioComMerenda: EnviarPostCardapioMerendaProvider,
		private postSoCardapio: EnviarPostCardapioProvider,
		private postSoMerenda: EnviarPostMerendaProvider) {

	}

	enviarPostagem(post) {
		return new Promise((resolve, reject) => {
			let retorno: any
			this.post = post
			if (Object.keys(this.post).length === 0 && this.post.constructor === Object) {
				reject("Erro")
			} else {
				if (this.post.temCardapio && this.post.temMerenda) {
					retorno = this.postComCardapioComMerenda.enviarPost(this.post)
				} else if (this.post.temCardapio && !this.post.temMerenda) {
					retorno = this.postSoCardapio.enviarPost(this.post)
				} else if (!this.post.temCardapio && this.post.temMerenda) {
					retorno = this.postSoMerenda.enviarPost(this.post)
				} else if (!this.post.temCardapio && !this.post.temMerenda) {
					retorno = this.postSemCardapioSemMerenda.enviarPost(this.post)
				}

				retorno.then( res => {
					resolve(res)
				}, err => {
					reject(err)
				})
			}
		})
	}

}

// ESTÁ PERMITINDO MAIS DE UMA POSTAGEM POR DIA PARA TESTES.
