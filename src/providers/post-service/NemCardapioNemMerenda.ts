import { Storage } from '@ionic/storage';
import { LoadingController, Loading, Events } from 'ionic-angular';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { Auth } from './../auth';
import { Utils } from './../utils';
import { Postagem } from './../postagem';
import { Http } from '@angular/http';

export class NemCardapioNemMerenda {

    token: string = null
    user: any
    post: any = {}
    hoje: any
    aluno: any
    escola: any
    postouHoje: any = null
    loading: Loading

    constructor(
        public http: Http,
        public events: Events,
        public postService: Postagem,
        public utils: Utils,
        public auth: Auth,
        public fileTransfer: TransferObject,
        public transfer: Transfer,
        public loadingCtrl: LoadingController,
        private storage: Storage
    ) {
        this.storage.ready().then( () => {
            this.storage.get('token').then(_token => {
                this.token = _token
            })

            this.storage.get('user').then(user => {
                this.user = user
            })
        })
    }

    public enviarPost(post) {
		// pontos = 30 + 100 = 130
		// temCardapio = false
		// temMerenda = false

        return new Promise( (resolve, reject) => {
            this.postService.jaPostouHoje().then(res => {
                this.hoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D HH:mm:ss');
                this.postouHoje = this.utils.momentFormatData(new Date(), 'YYYY-MM-D');
                this.escola = this.user.aluno.escola.esc_id;

                let url = this.auth.apiUrl + '/store-post';

                let targetPathCardapio = '';
                let filenameCardapio = '';

                this.auth.validToken(tokenValid => {
                    let options: FileUploadOptions = {
                        fileKey: "file",
                        fileName: filenameCardapio,
                        chunkedMode: false,
                        mimeType: "multipart/form-data",
                        headers: {
                            'Authorization': 'Bearer ' + tokenValid
                        },
                        params: {
                            'fileName': filenameCardapio,
                            'pos_date': this.hoje,
                            'aluno_id': this.user.id,
                            'escola_id': this.escola,
                            // 'conferir': this.post.merendaIgual,
                            // 'sabor': this.post.gostoMerenda,
                            'pontos': this.post.pontos,
                            'temCardapio': false,
                            'temMerenda': false
                        }
                    };

                    const fileTransfer: TransferObject = this.transfer.create();

                    this.loading = this.loadingCtrl.create({
                        content: 'Enviando...',
                    });
                    this.loading.present();

                    // Enviando a foto de nada
                    fileTransfer.upload(targetPathCardapio, url, options).then(res => {
                        this.loading.dismissAll()
                        this.utils.presentToast('Enviado com sucesso.');
                        this.storage.set('postouHoje', this.postouHoje)
                        this.events.publish('new:feed')
                        this.events.publish('refresh:perfil', tokenValid)
                        this.storage.remove('post')
                        resolve(res)
                    }, err => {
                        this.loading.dismissAll()
                        this.utils.presentToast('Erro ao enviar.');
                        reject(err)
                    });


                }) //this.auth.validToken() - foto de nada
            }, err => {
                this.utils.showAlert('Atenção!', 'Você já postou hoje.')
                reject()
            })
        })

    }

}